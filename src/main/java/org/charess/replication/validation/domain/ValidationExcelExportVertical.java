package org.charess.replication.validation.domain;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ValidationExcelExportVertical {
    private XSSFWorkbook workbook;
    private Map map;
    private ServletOutputStream outputStream ;
    private CellStyle style ;
    private XSSFFont font;
    private final String [] siteValue = new String[]{"iSante Value","iSantePlus Value", "isValid"};
    private Object [] objArr ;
    private XSSFSheet sheetIsante;
    private  XSSFSheet sheetIsanteplus;
    private XSSFSheet sheetResult;

    public ValidationExcelExportVertical(Map map, HttpServletResponse response, String siteName){
        this.map = map;
        this.workbook = new XSSFWorkbook();
        sheetIsante = workbook.createSheet(siteValue[0]);
        sheetIsanteplus = workbook.createSheet(siteValue[1]);
        sheetResult = workbook.createSheet(siteValue[2]);
        exportExcel(response,siteName);
    }
    public void writeHeaderLine(XSSFSheet sheet, XSSFWorkbook workbook, String value){
        int rownum = 0;
        Row row = sheet.createRow(rownum++);
        objArr = this.map.keySet().toArray(new Object[0]);
        this.style = workbook.createCellStyle();
        this.font = workbook.createFont();
        this.font.setBold(true);
        this.font.setFontHeight(8);
        this.style.setFont(font);
        this.style.setFillBackgroundColor(IndexedColors.BLUE_GREY.getIndex());
        int cellnum = 0;
        for (Object obj : objArr)
        {
            Cell cell = row.createCell(cellnum++);
            if(obj instanceof String){
                cell.setCellValue((String)obj);
                cell.setCellStyle(style);
            }
            else if(obj instanceof Integer){
                cell.setCellValue((Integer)obj);
                cell.setCellStyle(style);
            }
            else if (obj instanceof Date){
                String s =  (new SimpleDateFormat("yyyy-MM-dd")).format((Date)obj);
                cell.setCellValue((String) s);
                cell.setCellStyle(style);
            }
        }
    }
    public void writeLine(XSSFSheet sheet, String siteValue) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd");
        Set<String> patientSet = this.map.keySet();
        int rownum = 1;
        int cellnum = 0;
        int rowMax = 0;
        for(String key : patientSet) {
            rownum = 1;
            Object objArr = this.map.get(key);
            Object[] list = null;
            if (objArr instanceof ArrayList)
                list = ((ArrayList) objArr).toArray();
            else if (objArr instanceof HashSet)
                list = ((Set<Object>)objArr).toArray();
            Cell cell = null;
            Row row = null;
            for (Object obj : list) {
                if(rowMax == 0)
                {
                    row = sheet.createRow(rownum);
                    cell = row.createCell(cellnum);
                }
                else{
                    row = sheet.getRow(rownum);
                    cell = row.getCell(cellnum);
                }
                String val= null;
                try {
                    String[] tab = (String[]) obj;
                    val = tab[0] + "|" + tab[1];
                    if(this.siteValue[0].equals(siteValue))
                        val = tab[0];
                    else if(this.siteValue[1].equals(siteValue))
                        val = tab[1];
                    else
                        val = tab[0] +" | "+ tab[1];
                }catch (Exception e){
                    val = obj.toString();
                }
                cell = row.createCell(cellnum);
                if (val instanceof String) {
                        cell.setCellValue((String) val);
                }
                rownum++;
            }
            rowMax = rownum;
            cellnum ++;
        }
    }
    public void createSheet(HttpServletResponse response, String siteName) throws IOException {
        writeHeaderLine(sheetIsante, workbook,this.siteValue[0]);
        writeLine(sheetIsante,this.siteValue[0]);
        writeHeaderLine(sheetIsanteplus, workbook,this.siteValue[1]);
        writeLine(sheetIsanteplus, this.siteValue[1]);
        writeHeaderLine(sheetResult, workbook,this.siteValue[2]);
        writeLine(sheetResult, this.siteValue[2]);
        outputStream = response.getOutputStream();
        workbook.write(outputStream);
    }
    public void exportExcel(HttpServletResponse response, String siteName)  {
        response.setContentType("application/octet-stream");
        response.setCharacterEncoding("utf-8");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename="+siteName+".xlsx";
        response.setHeader(headerKey, headerValue);
        try {
            createSheet(response,siteName);
            outputStream.close();
        }catch (IOException e){
        }
    }
}
