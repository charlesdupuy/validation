package org.charess.replication.validation.service;

import org.charess.replication.validation.domain.Site;
import org.charess.replication.validation.repository.SiteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SiteServiceImpl implements SiteService {

    private SiteRepository siteRepository;

    @Autowired
    public SiteServiceImpl(SiteRepository siteRepository) {
        this.siteRepository = siteRepository;
    }

    public List<Site> listSites() {
        return this.siteRepository.findAll();
    }

    public Site getSiteById(Integer siteId) {
        return this.siteRepository.findById(siteId).get();
    }

    public Site getSiteByCode(Integer code){
        return this.siteRepository.findByCode(code);
    }

}
