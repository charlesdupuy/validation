package org.charess.replication.validation.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.charess.replication.validation.domain.Validation;
import org.charess.replication.validation.domain.ValidationExcelExport;
import org.charess.replication.validation.domain.ValidationExcelExportVertical;
import org.charess.replication.validation.service.SiteService;
import org.charess.replication.validation.service.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/export")
public class ExportValidationController{
    @Autowired
    private ValidationService validationService;
    @Autowired
    private SiteService siteService;

    @Operation(summary = "export to Excel validation horizontal")
    @RequestMapping(value = "/excelHorizontal", method = RequestMethod.GET)
    public ResponseEntity<?> exportToExcelHorizontal(HttpServletResponse response, @RequestParam(value="sitename") String siteName) throws IOException {
        List<Validation> validations = this.validationService.listValidationByCriteria(null);
        if(validations.size() > 0){
            ValidationExcelExport validationExcelExport = new ValidationExcelExport(validations, response, siteName);
        }
        return validations.size()>0?new ResponseEntity(null, HttpStatus.CREATED):new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "export to Excel patients vertical")
    @RequestMapping(value = "/excelVertical", method = RequestMethod.GET)
    public ResponseEntity<?> exportToExcelVertical(HttpServletResponse response, @RequestParam(value="siteId") Integer siteId, @RequestParam(value="forms", required = false) String[] forms,
                                                   @RequestParam(value="params", required = false) String[] parameters,
                                                   @RequestParam(value="start", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                                   @RequestParam(value="end", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
                                                   @RequestParam(value="min") Integer minPatientNumber,
                                                   @RequestParam(value="max") Integer maxPatientNumber) throws IOException {
        Map<String, Object> patients = this.validationService.getPatientsexport(siteId, forms, parameters, startDate, endDate, minPatientNumber, maxPatientNumber);
        ValidationExcelExportVertical validationExcelExport = new ValidationExcelExportVertical(patients, response, this.siteService.getSiteByCode(siteId).getName());
        return  patients.size()>0?new ResponseEntity(null, HttpStatus.CREATED):new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }
}
