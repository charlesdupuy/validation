jq = jQuery;
var context = '/replication';
loadSites();

function loadSites() {
    url = context + '/site/all';
    $.getJSON(url, function(sites) {
        $("#loadSearch").css("display", "none");
        $("#to_go").css("display", "none");
        opt(sites, '#sites', 'code', 'name');
    });
}

function loadFormsBySiteId(siteId){
    url = context + '/validation/form/site/'+siteId;
    $('#siteName').val($('#sites').find(":selected").text());
    $.getJSON(url, function(rs) {
        $('#forms').find('option').remove();
        opt(rs, '#forms')
    });
}

function loadParametersBySiteAndForms(){
    var site = $('#sites').val();
    var forms = $('#forms').val();
    url = context + '/validation/parameters?siteId='+site+'&forms='+forms;
    $.getJSON(url, function(rs) {
        $('#parameters').find('option').remove();
        opt(rs, '#parameters')
    });
}

function opt(array, key, val, txt){
    if(array && array.length > 0){
        array.forEach(
            function(v) {
                $(key).append(new Option(txt?v[txt]:v, val?v[val]:v));
            }
        );
    }
}

function move() {
    var i = 0;
    if (i == 0) {
        i = 1;
        var elem = document.getElementById("myBar");
        var progressValue = document.getElementById("progressValue");
        var width = 1;
        var id = setInterval(frame, 50);
        function frame() {
            if (width >= 100) {
                clearInterval(id);
                i = 0;
                if (width == 100){
                    $("#loadSearch").css("display", "none");
                    $("#myBar").css("display", "none");
                    $("#performValidation").css("display", "block");
                    $("#minPatientNumber").css("display", "block");
                    $("#maxPatientNumber").css("display", "block");
                }
            }
            else {
                width++;
                elem.style.width = width + "%";
                progressValue.innerHTML = width + "%";
            }
        }
    }
}

function displayLoadSearch() {
    $('#siteName').val($('#listSiteSelect').find(":selected").text());
    if ($('#loadSearch').val() != null){
        $("#loadSearch").css("display", "block");
        $("#myBar").css("display", "block");
        $("#performValidation").css("display", "none");
        $("#minPatientNumber").css("display", "none");
        $("#maxPatientNumber").css("display", "none");
        move();
    }
}

function go(){
    var code = $('#sites').val();
    var min = $('#minPatientNumber').val();
    var max = $('#maxPatientNumber').val();
    var name = $('#siteName').val();
    var forms = $('#forms').val();
    var parameters = $('#parameters').val();
    var startDate = fmt($('#startDate').val());
    var endDate = fmt($('#endDate').val());
    var display = $("input[name='display']:checked").val();
    var status = $("input[name='status']:checked").val();
    display = display==undefined?'vertical_output.html':display;
    status = status==undefined?'':status;
    var url = display+'?code='+code+'&min='+min+'&max='+max+'&name='+name+'&forms='+forms+'&parameters='+parameters+'&startDate='+startDate+'&endDate='+endDate+'&status='+status;
    location.replace(url);
}

function fmt(date){
    if(!date||date.length<0)
        return date;
    var d = new Date(date),  month = ''+(d.getMonth()+1), day = ''+d.getDate(), year = d.getFullYear();
    if(month.length < 2)
        month = '0'+month;
    if(day.length < 2)
        day = '0'+day;
    return [year, month, day].join('-');
}

