package org.charess.replication.validation.service;

import org.charess.replication.validation.domain.Key;
import org.charess.replication.validation.domain.Validation;
import org.charess.replication.validation.repository.ValidationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLOutput;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class ValidationServiceImpl implements ValidationService {

    private ValidationRepository validationRepository;
    private final Logger log = LoggerFactory.getLogger(ValidationServiceImpl.class);

    @Autowired
    public ValidationServiceImpl(ValidationRepository validationRepository) {
        this.validationRepository = validationRepository;
    }

    public List<Validation> listValidationByCriteria(String criteria) {
        List<Validation> validations = this.validationRepository.listValidationByCriteria(criteria==null?"":criteria);
        return validations;
    }

    public List<Validation> listValidationByPatientId(Integer patientId) {
        List<Validation> validations = this.validationRepository.findValidationByPatientId(patientId);
        return validations;
    }

    public List<Validation> listValidationBySiteId(Integer siteId) {
        return this.validationRepository.findValidationBySiteIdOrderByPatientIdAsc(siteId);
    }

    public List<Validation> listValidationByFormOrigin(String formOrigin) {
        return this.validationRepository.findValidationByFormOrigin(formOrigin);
    }

    public List<Validation> listValidationByParameter(String parameter) {
        return this.validationRepository.findValidationByParameter(parameter);
    }

    public Page<Validation> pageValidationByCriteria(String criteria, Pageable pageable) {
        return this.validationRepository.pageValidationByCriteria(criteria, pageable);
    }

    public List<Validation> listValidationsByFacilityAndPatientRange(Integer siteId, Integer minPatientNumber, Integer maxPatientNumber){
        List<Validation> validations = this.validationRepository.findValidationBySiteIdOrderByPatientIdAsc(siteId);
        return  validations.size()>0?validations.subList(minPatientNumber<1?1:minPatientNumber, maxPatientNumber>validations.size()?validations.size():maxPatientNumber):validations;
    }

    public Set<String> getDistinctFormsBySite(Integer siteId){
        return this.validationRepository.findValidationBySiteIdOrderByPatientIdAsc(siteId)
                .stream().map(v-> v.getFormOrigin()).collect(Collectors.toSet());
    }

    private Set<String> getPatientsIdsBySiteId(Integer siteId){
        return this.validationRepository.findValidationBySiteIdOrderByPatientIdAsc(siteId)
                .stream().map(v-> v.getPatientId()).collect(Collectors.toSet());
    }

    public Set<String> getDistinctParametersBySite(Integer siteId, String[] forms){
        return this.validationRepository.getDistinctParametersBySiteAndForms(siteId, forms)
                .stream().map(v-> v.getParameter()).collect(Collectors.toSet());
    }

    private Map<String, Set> getValidationFieldValues(Integer siteId){
        Map<String, Set> map = new HashMap<String, Set>();
        Set<String> forms = new HashSet<String>();
        Set<String> patientIds = new HashSet<String>();
        this.validationRepository.findValidationBySiteIdOrderByPatientIdAsc(siteId)
                .stream().map(v-> {
            forms.add(v.getFormOrigin());
            patientIds.add(v.getPatientId());
            return v;
        });
        map.put("forms", forms);
        map.put("patientIds", patientIds);
        return map;
    }

    public List<Validation> listValidationsBySiteAndFormsAndParametersAndCollectionDateAndPatients(Integer siteId, String[] forms, String[] parameters, LocalDate startDate,
                                                                                                   LocalDate endDate, Integer minPatientNumber, Integer maxPatientNumber){
        Set<String> patientIds = this.getPatientsIdsBySiteId(siteId);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String date = "1900-01-01";
        LocalDate oldDate = LocalDate.parse(date, formatter);

        int totalPatients = patientIds.size();
        maxPatientNumber = maxPatientNumber==null || maxPatientNumber<1 || maxPatientNumber<=minPatientNumber || maxPatientNumber>totalPatients?totalPatients:maxPatientNumber;
        minPatientNumber = minPatientNumber==null || minPatientNumber<1?1:minPatientNumber;
        -- minPatientNumber;
        if(forms == null || forms.length < 1)
            forms = this.getDistinctFormsBySite(siteId).toArray(new String[0]);
        if(parameters == null || parameters.length < 1)
            parameters = this.getDistinctParametersBySite(siteId, forms).toArray(new String[0]);
        if(startDate==null)
            startDate = oldDate;
        if(endDate==null ||endDate.isBefore(startDate))
            endDate = LocalDate.now();
        List<Validation> validations = this.validationRepository.listValidationsBySiteAndFormsAndParametersAndCollectionDateAndPatientRange(siteId,
                forms, parameters, startDate, endDate, new ArrayList<String>(patientIds).subList(minPatientNumber, maxPatientNumber));
        return  validations;
    }

    public Map<String, List<Map>> getPatients(Integer siteId, String[] forms, String[] parameters, LocalDate startDate,
                                              LocalDate endDate, Integer minPatientNumber, Integer maxPatientNumber, String action) {
        Map<String, Object> fm = new LinkedHashMap<>();
        Map<String, Object> map = new HashMap<>();
        Map<String, List<Map>> validationsData = new HashMap<>();
        Map<String, List<Map>> validationsDataOrd = new HashMap<>();

        String[] arrayValue = new String[3];
        String parameter = "";
        int total = 0;
        int counter = 0;
        Set<String> ids = new LinkedHashSet<>();
        List<String[]> obj = new ArrayList<>();
        Set<String> params = new LinkedHashSet<>();
        Map<String, String[]> mapVal = new HashMap<>();
        Map<String, String> mapValHeader = new HashMap<>();
        final String header = "header";
        List<Validation> validations = this.listValidationsBySiteAndFormsAndParametersAndCollectionDateAndPatients(siteId, forms,
                parameters, startDate, endDate, minPatientNumber, maxPatientNumber);

        int i = 0;
        for (Validation v : validations) {
            params.add(v.getParameter());
            if (i == 0)
                parameter = v.getParameter();
            if (parameter.equals(v.getParameter())) {
                ++total;
            }
            if (parameter.equals(v.getParameter()) && "y".equals(v.getIsValid())) {
                ++counter;
            }
            Set<String> setValidationData = validationsData.keySet();
            final List<Map> listMap = new LinkedList<>();
            if (action == null) {
                arrayValue[0] = "<span style='color:blue;'>(" + v.getIsanteValue() + ")</span>";
                arrayValue[1] = "<span style='color:black;'>(" + v.getIsanteplusValue() + ")</span>";
                arrayValue[2] = "<img src='images/" + ("y".equals(v.getIsValid()) ? "good" : ("x".equals(v.getIsValid()) ? "minus" : "wrong")) + ".png' width='15px' height='15px'/>";
            }
            mapVal.put(v.getParameter(), arrayValue);
            listMap.add(mapVal);
            final List<Map> listHeader = new LinkedList<>();
            if (total > 0) {
                mapValHeader.put(v.getParameter(), (counter * 100 / total) + "%");
                listHeader.add(mapValHeader);
            }
            if (!parameter.equals(v.getParameter())) {
                parameter = v.getParameter();
                total = 0;
                counter = 0;
                i = 0;
            }
            ++i;
            validationsData.computeIfAbsent(header, s -> listHeader);
            validationsData.computeIfPresent(header, (k, list) -> listHeader);
            validationsData.computeIfAbsent(v.getPatientId(), s -> listMap);
            final List<Map> listMapV = validationsData.get(v.getPatientId());
            if (!listMapV.contains(mapVal))
                listMapV.add(mapVal);
            validationsData.computeIfPresent(v.getPatientId(), (k, list) -> listMapV);
            arrayValue = new String[3];
            mapVal = new HashMap<>();
        }
        arrayValue = new String[1];
        boolean test = false;
        List<Map> listM = validationsData.get(header);
        validationsData.remove(header);
        validationsData.put(header,ordListMap(listM,null));
        for (Map m : listM) {
            Set<String> set = m.keySet();
            List<String> list = new ArrayList<String>(set);
            for (String key : list) {
                    Set<String> idsP = validationsData.keySet();
                    for (String id : idsP) {
                        String[] arrayValueLine = new String[3];
                        List<Map> listMP = validationsData.get(id);
                        for (Map mp : listMP) {
                            Set<String> setmap = mp.keySet();
                            for (String keyP : setmap) {
                                if (keyP.equals(key)) {
                                    test = true;
                                }
                            }
                        }
                        if (!test) {
                            final List<Map> listMapVv = validationsData.get(id);
                            arrayValueLine = new String[3];
                            arrayValueLine[0] = "<span style='color:blue;'>(null)</span>";
                            arrayValueLine[1] = "<span style='color:black;'>(null)</span>";
                            arrayValueLine[2] = "<img src='images/minus.png' width='15px' height='15px'/>";
                            Map<String, String[]> mapValNullV = new HashMap<>();
                            mapValNullV.put(key, arrayValueLine);
                            listMapVv.add(mapValNullV);
                            validationsData.computeIfPresent(id, (k, v) -> listMapVv);
                        }
                        test = false;
                        arrayValueLine = new String[3];
                    }
            }
        }
        Set<String> setM = validationsData.keySet();
        List<Map> listHeaderOrd = validationsData.get(header);
        List <String> arrLis =new LinkedList<>();
        for (Map mOrd : listHeaderOrd) {
            Set<String> set = mOrd.keySet();
            List<String> list = new ArrayList<String>(set);
            for(String s: list)
                arrLis.add(s);
        }
        for(String idd: setM){
            validationsDataOrd.put(idd,ordListMap(validationsData.get(idd),arrLis));
        }
        return validationsDataOrd;
    }


    public List<Map> ordListMap(List<Map> listM, List<String> header){
        final List<Map> listHeaderOrd = new LinkedList<>();
        Map<String, String[]> mapValord = new HashMap<>();
        String [] arrayValue = new String[]{};
        if(header!=null){
            for(String s : header) {
                for (Map m : listM) {
                    if(m.containsKey(s))
                        listHeaderOrd.add(m);
                }
            }
            return listHeaderOrd;
        }
        for (Map m : listM) {
            Set<String> set = m.keySet();
            List<String> list = new ArrayList<String>(set);
            Collections.sort(list);
            for (String key : list) {
                try{
                    arrayValue=(String[]) m.get(key);
                }catch (Exception exception){
                    arrayValue = new String[]{m.get(key).toString()};
                }
                mapValord.put(key,arrayValue);
                listHeaderOrd.add(mapValord);
                mapValord = new HashMap<>();
                arrayValue = new String[]{};
            }
        }
        return listHeaderOrd;
    }

    public Map<String, Object> getPatientsexport(Integer siteId, String[] forms, String[] parameters, LocalDate startDate,
                                           LocalDate endDate, Integer minPatientNumber, Integer maxPatientNumber){
        Map<String, Object> fm = new LinkedHashMap<>();
        Map<String, Object> map = new HashMap<>();
        String parameter = "";
        int total=0;
        int counter=0;
        Set<String> ids = new LinkedHashSet<>();
        Set<String[]> obj = null;
        List<Validation> validations = this.listValidationsBySiteAndFormsAndParametersAndCollectionDateAndPatients(siteId, forms,
                parameters, startDate, endDate, minPatientNumber, maxPatientNumber);
        for(Validation v: validations) {
            if (!parameter.equals(v.getParameter())) {
                if (parameter.length() > 0)
                    map.put(parameter, obj);
                obj = new LinkedHashSet<String[]>();
                parameter = v.getParameter();
                total = 0;
                counter = 0;
            }
            ids.add(v.getPatientId().toString());
            obj.add(new String[]{v.getIsanteValue(), v.getIsanteplusValue()} );
            ++total;
            counter += "y".equals(v.getIsValid())?1:0;
            Object [] objAr = null;
            for(String [] key : obj) {
                objAr = key;
            }
        }
        map.put(parameter, obj);
        fm.put("id", ids);
        fm.putAll(map);
        return fm;
    }

}
