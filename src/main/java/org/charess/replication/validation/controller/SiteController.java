package org.charess.replication.validation.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.charess.replication.validation.domain.Site;

import org.charess.replication.validation.service.SiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/site")
public class SiteController {

    private SiteService siteService;

    @Autowired
    public SiteController(SiteService siteService) {
        this.siteService = siteService;
    }

    @Operation(summary = "list all sites")
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<?> listSites() {
        List<Site> sites = this.siteService.listSites();
        return sites.size()>0?ResponseEntity.ok(sites):new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }
}
