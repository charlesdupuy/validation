package org.charess.replication.validation.repository;

import org.charess.replication.validation.domain.Site;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SiteRepository extends JpaRepository<Site, Integer> {

    Site findByCode(Integer code);
}
