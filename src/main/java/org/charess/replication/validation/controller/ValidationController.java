package org.charess.replication.validation.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.charess.replication.validation.domain.Validation;
import org.charess.replication.validation.service.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.*;

@RestController
@RequestMapping("/validation")
public class ValidationController {

    private ValidationService validationService;
    private final Logger log = LoggerFactory.getLogger(ValidationController.class);

    @RequestMapping("/index")
    public String homeView() {
        return "index";
    }

    @Autowired
    public ValidationController(ValidationService validationService) {
        this.validationService = validationService;
    }

    @Operation(summary = "list all records for all parameters")
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<?> listValidation() {
        List<Validation> validations = this.validationService.listValidationByCriteria(null);
        return validations.size()>0?ResponseEntity.ok(validations):new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<?> searchValidations(@RequestParam(value = "criteria") String criteria) {
        List<Validation> validations = this.validationService.listValidationByCriteria(criteria);
        return validations.size()>0?ResponseEntity.ok(validations):new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/formOrigin/{formOrigin}", method = RequestMethod.GET)
    public ResponseEntity<?> getValidationsByFormOrigin(@PathVariable String formOrigin) {
        List<Validation> validations = this.validationService.listValidationByFormOrigin(formOrigin);
        return validations.size()>0?ResponseEntity.ok(validations):new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/parameter/{parameter}", method = RequestMethod.GET)
    public ResponseEntity<?> getValidationsByParameter(@PathVariable String parameter) {
        List<Validation> validations = this.validationService.listValidationByParameter(parameter);
        return validations.size()>0?ResponseEntity.ok(validations):new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/siteId/{siteId}", method = RequestMethod.GET)
    public ResponseEntity<?> getValidationsByFormOrigin(@PathVariable Integer siteId) {
        List<Validation> validations = this.validationService.listValidationBySiteId(siteId);
        return validations.size()>0?ResponseEntity.ok(validations):new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/patientId/{patientId}", method = RequestMethod.GET)
    public ResponseEntity<?> getValidationsByPatientId(@PathVariable Integer patientId) {
        List<Validation> validations = this.validationService.listValidationByPatientId(patientId);
        return validations.size()>0?ResponseEntity.ok(validations):new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public Page<Validation> searchValidations(Pageable pageable, @RequestParam(value = "criteria", required = false) String criteria) {
        return this.validationService.pageValidationByCriteria(criteria, pageable);
    }

    @RequestMapping(value = "/site/{siteId}/patient-range/{minPatientNumber}/{maxPatientNumber}", method = RequestMethod.GET)
    public ResponseEntity<?> listValidationsByFacilityAndPatientRange(@PathVariable Integer siteId, @PathVariable Integer minPatientNumber,
                                                                      @PathVariable Integer maxPatientNumber) {
        List<Validation> validations = this.validationService.listValidationsByFacilityAndPatientRange(siteId, minPatientNumber, maxPatientNumber);
        return validations.size()>0?ResponseEntity.ok(validations):new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "list all the data collection forms by site id")
    @RequestMapping(value = "/form/site/{siteId}", method = RequestMethod.GET)
    public ResponseEntity<?> listDistinctFormsBySite(@PathVariable Integer siteId) {
        Set<String> forms = this.validationService.getDistinctFormsBySite(siteId);
        return forms.size()>0?ResponseEntity.ok(forms):new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }
    @Operation(summary = "list all the data collection parameters by forms and site id")
    @RequestMapping(value = "/parameters", method = RequestMethod.GET)
    public ResponseEntity<?> listParametersByFormsAndSite(@RequestParam(value="siteId") Integer siteId, @RequestParam(value="forms", required = false) String[] forms) {
        Set<String> parameters = this.validationService.getDistinctParametersBySite(siteId,forms);
        return parameters.size()>0?ResponseEntity.ok(parameters):new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "list the validations by site, forms, parameters, collection interval and patient range")
    @RequestMapping(value = "/filter", method = RequestMethod.GET)
    public ResponseEntity<?> filter(@RequestParam(value="siteId") Integer siteId, @RequestParam(value="forms", required = false) String[] forms,
                                    @RequestParam(value="params", required = false) String[] parameters,
                                    @RequestParam(value="start", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                    @RequestParam(value="end", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
                                    @RequestParam(value="min") Integer minPatientNumber,
                                    @RequestParam(value="max") Integer maxPatientNumber){
        List<Validation> validations = this.validationService.listValidationsBySiteAndFormsAndParametersAndCollectionDateAndPatients(siteId, forms, parameters, startDate, endDate, minPatientNumber, maxPatientNumber);
        return validations.size()>0?ResponseEntity.ok(validations):new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }


    @Operation(summary = "list the values by site, forms, parameters, collection interval and patient range")
    @RequestMapping(value = "/patients", method = RequestMethod.GET)
    public ResponseEntity<?> getPatients(@RequestParam(value="siteId") Integer siteId, @RequestParam(value="forms", required = false) String[] forms,
                                         @RequestParam(value="params", required = false) String[] parameters,
                                         @RequestParam(value="start", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                         @RequestParam(value="end", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
                                         @RequestParam(value="min") Integer minPatientNumber,
                                         @RequestParam(value="max") Integer maxPatientNumber){
        Map<String, List<Map>> patients = this.validationService.getPatients(siteId, forms, parameters, startDate, endDate, minPatientNumber, maxPatientNumber,null);
        return patients.size()>0?ResponseEntity.ok(patients):new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }
}
