package org.charess.replication.validation.repository;

import org.charess.replication.validation.domain.Key;
import org.charess.replication.validation.domain.Validation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface ValidationRepository extends CrudRepository<Validation, Key> {

    @Query("select v from Validation v " +
            "where v.formOrigin like Concat('%', Concat(?1,'%')) " +
            "or v.parameter like Concat('%', Concat(?1,'%')) " +
            "or v.stCode like Concat('%', Concat(?1,'%'))")
    Page<Validation> pageValidationByCriteria(String criteria, Pageable pageable);

    @Query("select v from Validation v " +
            "where v.formOrigin like Concat('%', Concat(?1,'%')) " +
            "or v.parameter like Concat('%', Concat(?1,'%')) " +
            "or v.stCode like Concat('%', Concat(?1,'%'))")
    List<Validation> listValidationByCriteria(String criteria);

    List<Validation> findValidationByPatientId(Integer patientId);

    List<Validation> findValidationByFormOrigin(String formOrigin);

    List<Validation> findValidationBySiteIdOrderByPatientIdAsc(Integer siteId);

    List<Validation> findValidationByParameter(String parameter);

    @Query("select v from Validation v " +
            "where v.siteId = ?1 " +
            "and v.formOrigin in (?2) ")
    List<Validation> getDistinctParametersBySiteAndForms(Integer siteId, String[] forms);

    @Query("select v from Validation v " +
            "where v.siteId = ?1 " +
            "and v.formOrigin in (?2) " +
            "and v.parameter in (?3) " +
            "and v.createdDate between ?4 and ?5 " +
            "and v.patientId in ?6 " +
            "order by v.formOrigin asc, v.parameter asc, v.patientId asc ")
    List<Validation> listValidationsBySiteAndFormsAndParametersAndCollectionDateAndPatientRange(Integer siteId, String[] forms, String[] parameters, LocalDate startDate,
                                                                                                LocalDate endDdate, List<String> patientIds);
}