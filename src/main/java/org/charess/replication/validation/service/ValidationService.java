package org.charess.replication.validation.service;

import org.charess.replication.validation.domain.Validation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ValidationService {

    List<Validation> listValidationByCriteria(String criteria);
    List<Validation> listValidationByPatientId(Integer patientId);
    List<Validation> listValidationBySiteId(Integer patientId);
    List<Validation> listValidationByFormOrigin(String formOrigin);
    List<Validation> listValidationByParameter(String parameter);
    Page<Validation> pageValidationByCriteria(String criteria, Pageable pageable);
    List<Validation> listValidationsByFacilityAndPatientRange(Integer siteId, Integer minPatientNumber, Integer maxPatientNumber);
    Set<String> getDistinctFormsBySite(Integer siteId);
    Set<String> getDistinctParametersBySite(Integer siteId, String[] forms);

    List<Validation> listValidationsBySiteAndFormsAndParametersAndCollectionDateAndPatients(Integer siteId, String[] forms, String[] parameters, LocalDate startDate,
                                                                                            LocalDate endDdate, Integer minPatientNumber, Integer maxPatientNumber);
    Map<String, List<Map>> getPatients(Integer siteId, String[] forms, String[] parameters, LocalDate startDate,
                                       LocalDate endDate, Integer minPatientNumber, Integer maxPatientNumber, String action);
    Map<String, Object> getPatientsexport(Integer siteId, String[] forms, String[] parameters, LocalDate startDate,
                                                 LocalDate endDate, Integer minPatientNumber, Integer maxPatientNumber);
}
