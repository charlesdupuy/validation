jq = jQuery;
var context = '/replication';
var params = (new URL(document.location)).searchParams;

var code = params.get("code");
var name = params.get("name");
var forms = params.get("forms");
var start = params.get("startDate");
var end = params.get("endDate");
var end = params.get("endDate");
var parameters = params.get("parameters");
var min = params.get("min");
var max = params.get("max");
url = context +'/validation/patients?siteId='+code+'&forms='+forms+'&params='+parameters+'&start='+start+'&end='+end+'&min='+min+'&max='+max;

loadValidations();
/*
function loadValidations() {
    if (code == null)
        location.replace('index.html');
    document.getElementById('pageTitle').innerHTML = name + ' - Patient: [' + min + ', ' + max + '] - Form: ' + forms.split(',')[0] + ' - Collection Date: ' + start;
    if (code && min && max) {
        $.getJSON(url, function (rs) {
            console.log(rs, 1);
            var data=[];
            $(function () {
                $.each(rs, function (k, v) {
                    var $th = $('<th>').append(k).appendTo('#vThead');
                    $.each(v, function (i, e) {
                        if (data[i]){
                            data[i].push(e);
                        } else {
                            data[i] = [];
                            data[i].push(e);
                        }
                    });
                });
                $.each(data, function(i, arr){
                    var td = '';
                    $.each(arr, function(j, e){
                        if(Array.isArray(e))
                            e = e.join(" ");
                        td += '<td>'+ e +'</td>';
                    });
                    var $tr = $('<tr>').append(td).appendTo("#vTbody");
                });
            });
        });
    }
}*/

function loadValidations() {
    var centerTextPlugin = {
        afterDatasetsUpdate: function (chart) { },
        beforeDraw: function (chart) {
            var width = chart.chartArea.right;
            var height = chart.chartArea.bottom;
            var ctx = chart.chart.ctx;
            ctx.restore();

            var activeLabel = chart.data.labels[0];
            var activeValue = chart.data.datasets[0].data[0];
            var dataset = chart.data.datasets[0];
            var meta = dataset._meta[Object.keys(dataset._meta)[0]];
            var total = meta.total;

            var activePercentage = parseFloat(
                ((activeValue / total) * 100).toFixed(1)
            );
            activePercentage = chart.legend.legendItems[0].hidden
                ? 0
                : activePercentage;

            if (chart.pointAvailable) {
                activeLabel = chart.data.labels[chart.pointIndex];
                activeValue =
                    chart.data.datasets[chart.pointDataIndex].data[chart.pointIndex];

                dataset = chart.data.datasets[chart.pointDataIndex];
                meta = dataset._meta[Object.keys(dataset._meta)[0]];
                total = meta.total;
                activePercentage = parseFloat(
                    ((activeValue / total) * 100).toFixed(1)
                );
                activePercentage = chart.legend.legendItems[chart.pointIndex].hidden
                    ? 0
                    : activePercentage;
            }

            ctx.font = "36px" + " Nunito, sans-serif";
            ctx.fillStyle = primaryColor;
            ctx.textBaseline = "middle";

            var text = activePercentage + "%",
                textX = Math.round((width - ctx.measureText(text).width) / 2),output
            textY = height / 2;
            ctx.fillText(text, textX, textY);

            ctx.font = "14px" + " Nunito, sans-serif";
            ctx.textBaseline = "middle";

            var text2 = activeLabel,
                textX = Math.round((width - ctx.measureText(text2).width) / 2),
                textY = height / 2 - 30;
            ctx.fillText(text2, textX, textY);

            ctx.save();
        },
        beforeEvent: function (chart, event, options) {
            var firstPoint = chart.getElementAtEvent(event)[0];

            if (firstPoint) {
                chart.pointIndex = firstPoint._index;
                chart.pointDataIndex = firstPoint._datasetIndex;
                chart.pointAvailable = true;
            }
        }
    };

    if (code == null)
        location.replace('index.html');
    document.getElementById('pageTitle').innerHTML = name + ' - Patient: [' + min + ', ' + max + '] - Form: ' + forms.split(',')[0] + ' - Collection Date: ' + start;
    if (code && min && max) {
        $.getJSON(url, function (rs) {
            var data=[];
            var header=[];
            var v = rs["header"];
            header["IdPatient"]="IdPatient";
            var $th = $('<th>').append(header["IdPatient"]).appendTo('#vThead');
            $.each(v,function (i,header) {
                header = v[i];
                $.each(header,function (j){
                    var $th= $('<th>').append(j+header[j]).appendTo('#vThead');
                });
            });
            $.each(rs, function (k, v) {
                if (k!="header") {
                    var td = '<td>' + k + '</td>';
                    $.each(v, function (i) {
                        v.sort();
                        var line = v[i];
                        $.each(line,function (j){
                            td += '<td>'+line[j][0]+'|'+line[j][1]+line[j][2]+'</td>';
                        });
                    });
                    var $tr = $('<tr>').append(td).appendTo("#vTbody");
                }
            });
        });
    }
}

function exportToExcel(){
    url = context + '/export/excelVertical?siteId='+code+'&forms='+forms+'&params='+parameters+'&start='+start+'&end='+end+'&min='+min+'&max='+max
    window.location.href=window.location['origin']+url;
}

