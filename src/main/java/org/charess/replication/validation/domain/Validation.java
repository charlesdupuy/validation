package org.charess.replication.validation.domain;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "validation")
@IdClass(Key.class)
public class Validation implements Serializable {

    @Id
    @Column(name = "site_id", nullable = false, length = 11)
    private Integer siteId;

    @Id
    @Column(name = "patient_id", nullable = false, length = 11)
    private String patientId;

    @Id
    @Column(name = "form_origin", nullable = false, length = 100)
    private String formOrigin;

    @Id
    @Column(name = "parameter", nullable = false, length = 200)
    private String parameter;

    @Id
    @Column(name = "created_date", nullable = false)
    private LocalDate createdDate;

    @Column(name = "st_code", nullable = false, length = 40)
    private String stCode;

    @Column(name = "isante_value", length = 400)
    private String isanteValue;

    @Column(name = "isanteplus_value", length = 400)
    private String isanteplusValue;

    @Column(name = "is_valid", nullable = false, length = 1)
    private String isValid;

    @Column(name = "version", nullable = false, length = 1)
    private LocalDate version;

    public Integer getSiteId() {
        return siteId;
    }

    public void setSiteId(Integer siteId) {
        this.siteId = siteId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getFormOrigin() {
        return formOrigin;
    }

    public void setFormOrigin(String formOrigin) {
        this.formOrigin = formOrigin;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getStCode() {
        return stCode;
    }

    public void setStCode(String stCode) {
        this.stCode = stCode;
    }

    public String getIsanteValue() {
        return isanteValue;
    }

    public void setIsanteValue(String isanteValue) {
        this.isanteValue = isanteValue;
    }

    public String getIsanteplusValue() {
        return isanteplusValue;
    }

    public void setIsanteplusValue(String isanteplusValue) {
        this.isanteplusValue = isanteplusValue;
    }

    public String getIsValid() {
        return isValid;
    }

    public void setIsValid(String isValid) {
        this.isValid = isValid;
    }

    public LocalDate getVersion() {
        return version;
    }

    public void setVersion(LocalDate version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Validation)) return false;
        Validation that = (Validation) o;
        return Objects.equals(getSiteId(), that.getSiteId()) &&
            Objects.equals(getPatientId(), that.getPatientId()) &&
            Objects.equals(getFormOrigin(), that.getFormOrigin()) &&
            Objects.equals(getParameter(), that.getParameter()) &&
            Objects.equals(getCreatedDate(), that.getCreatedDate()) &&
            Objects.equals(getStCode(), that.getStCode()) &&
            Objects.equals(getIsanteValue(), that.getIsanteValue()) &&
            Objects.equals(getIsanteplusValue(), that.getIsanteplusValue()) &&
            Objects.equals(getIsValid(), that.getIsValid()) &&
            Objects.equals(getVersion(), that.getVersion());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSiteId(), getPatientId(), getFormOrigin(), getParameter(), getCreatedDate(), getStCode(), getIsanteValue(), getIsanteplusValue(), getIsValid(), getVersion());
    }
}
