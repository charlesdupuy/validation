jq = jQuery;
var context = '/replication';
var params = (new URL(document.location)).searchParams;

var code = params.get("code");
var name = params.get("name");
var forms = params.get("forms");
var start = params.get("startDate");
var end = params.get("endDate");
var parameters = params.get("parameters");
var min = params.get("min");
var max = params.get("max");
url = context +'/validation/filter?siteId='+code+'&forms='+forms+'&params='+parameters+'&start='+start+'&end='+end+'&min='+min+'&max='+max;


loadValidations();
function loadValidations(){
    if(code == null)
        location.replace('index.html');
    document.getElementById('pageTitle').innerHTML = name+' - Patient ['+min+', '+ max+']';
    if(code && min && max){
        $.getJSON(url, function(rs){
            $(function() {
                $.each(rs, function(i, v) {
                    var $tr = $('<tr>').append(
                        $('<td>').text(v.patientId),
                        $('<td>').append(v.stCode),
                        $('<td>').append(v.formOrigin),
                        $('<td>').append(v.parameter),
                        $('<td>').append(v.createdDate),
                        $('<td>').append(v.isanteValue),
                        $('<td>').append(v.isanteplusValue),
                        $('<td>').append(
                            $('<img>', {src: 'images/'+(v.isValid=='y'?'good':('x'==v.isValid?'minus':'wrong'))+'.png', width:'15px', height:'15px'}))
                    ).appendTo('#validationsTable');
                });
            });
        });
    }
}

function exportToExcel(){
    url = context + '/export/excelHorizontal?sitename='+name
    window.location.href=window.location['origin']+url;
}