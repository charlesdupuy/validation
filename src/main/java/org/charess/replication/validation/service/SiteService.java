package org.charess.replication.validation.service;

import org.charess.replication.validation.domain.Site;

import java.util.List;

public interface SiteService {
    List<Site> listSites();
    Site getSiteById(Integer siteId);
    Site getSiteByCode(Integer code);
}
