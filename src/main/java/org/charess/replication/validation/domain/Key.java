package org.charess.replication.validation.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class Key implements Serializable {

    private Integer siteId;

    private String patientId;

    private String formOrigin;

    private String parameter;

    private LocalDate createdDate;

    public Key() {
    }

    public Key(Integer siteId, String patientId, String formOrigin, String parameter, LocalDate createdDate) {
        this.siteId = siteId;
        this.patientId = patientId;
        this.formOrigin = formOrigin;
        this.parameter = parameter;
        this.createdDate = createdDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Key)) return false;
        Key key = (Key) o;
        return Objects.equals(siteId, key.siteId) &&
                Objects.equals(patientId, key.patientId) &&
                Objects.equals(formOrigin, key.formOrigin) &&
                Objects.equals(parameter, key.parameter) &&
                Objects.equals(createdDate, key.createdDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(siteId, patientId, formOrigin, parameter, createdDate);
    }
}
