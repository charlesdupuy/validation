package org.charess.replication.validation.domain;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ValidationExcelExport {
    private XSSFWorkbook workbook;
    private List<Validation> listValidation;
    private ServletOutputStream outputStream ;
    private CellStyle style ;
    private XSSFFont font;
    private final String [] siteValue = new String[]{"iSante Value","iSantePlus Value", "isValid"};
    private Object [] objArr ;
    private XSSFSheet sheetIsante;
    private XSSFSheet sheetIsanteplus;
    private XSSFSheet sheetResult;

    public ValidationExcelExport(List listValidation, HttpServletResponse response, String siteName){
        this.listValidation = listValidation;
        this.workbook = new XSSFWorkbook();
        sheetIsante = workbook.createSheet(siteValue[0]);
        sheetIsanteplus = workbook.createSheet(siteValue[1]);
        sheetResult = workbook.createSheet(siteValue[2]);
        exportExcel(response,siteName);
    }

    public void writeHeaderLine(XSSFSheet sheet, XSSFWorkbook workbook, String value){
        int rownum = 0;
        Row row = sheet.createRow(rownum++);
        if(value.equals(siteValue[2]))
           objArr = new String[]{"Patient Id","Site Id","Form Origin","Parameter","Created Date","St Code",siteValue[0],siteValue[1],"isValid","Version"};
        else
            objArr = new String[]{"Patient Id","Site Id","Form Origin","Parameter","Created Date","St Code",value,"Version"};
        this.style = workbook.createCellStyle();
        this.font = workbook.createFont();
        this.font.setBold(true);
        this.font.setFontHeight(8);
        this.style.setFont(font);
        this.style.setFillBackgroundColor(IndexedColors.BLUE_GREY.getIndex());

        int cellnum = 0;

        for(int i = 0 ; i < objArr.length ; i++) {
            Cell cell = row.createCell(cellnum++);
            if(objArr[i] instanceof String){
                cell.setCellValue((String)objArr[i]);
                cell.setCellStyle(style);
            } else if(objArr[i] instanceof Integer){
                cell.setCellValue((Integer)objArr[i]);
                cell.setCellStyle(style);
            } else if (objArr[i] instanceof Date){
                String s =  (new SimpleDateFormat("yyyy-MM-dd")).format((Date)objArr[i]);
                cell.setCellValue((String) s);
                cell.setCellStyle(style);
            }
        }
    }

    public void writeLine(XSSFSheet sheet, String siteValue) {
        Map<String, Object[]> data = new HashMap<String, Object[]>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd");
        for (Validation v : this.listValidation) {
            if(siteValue.equals(this.siteValue[2]))
                data.put(v.getCreatedDate().toString(), new Object[]{v.getPatientId(), v.getSiteId(), v.getFormOrigin(), v.getParameter(), formatter.format(v.getCreatedDate()),
                    v.getStCode(),v.getIsanteValue(),v.getIsanteplusValue(), v.getIsValid(), formatter.format(v.getVersion())});
            else
            data.put(v.getCreatedDate().toString(), new Object[]{v.getPatientId(), v.getSiteId(), v.getFormOrigin(), v.getParameter(), formatter.format(v.getCreatedDate()),
                    v.getStCode(),siteValue.equals(this.siteValue[0])?v.getIsanteValue():v.getIsanteplusValue(), formatter.format(v.getVersion())});
        }
        Set<String> keyset = data.keySet();
        int rownum = 1;
        for (String key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (int i = 0; i < objArr.length; i++) {
                Cell cell = row.createCell(cellnum++);
                if (objArr[i] instanceof String)
                    cell.setCellValue((String) objArr[i]);
                else if (objArr[i] instanceof Integer)
                    cell.setCellValue((Integer) objArr[i]);
            }
        }
    }
    public void createSheet(HttpServletResponse response, String siteName) throws IOException {
        writeHeaderLine(sheetIsante, workbook,this.siteValue[0]);
        writeLine(sheetIsante,this.siteValue[0]);
        writeHeaderLine(sheetIsanteplus, workbook,this.siteValue[1]);
        writeLine(sheetIsanteplus, this.siteValue[1]);
        writeHeaderLine(sheetResult, workbook,this.siteValue[2]);
        writeLine(sheetResult, this.siteValue[2]);
        outputStream = response.getOutputStream();
        workbook.write(outputStream);
    }
    public void exportExcel(HttpServletResponse response, String siteName)  {
        response.setContentType("application/octet-stream");
        response.setCharacterEncoding("utf-8");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename="+siteName+".xlsx";
        response.setHeader(headerKey, headerValue);
        try {
            createSheet(response,siteName);
            outputStream.close();
        }catch (IOException e){
        }
    }
}
