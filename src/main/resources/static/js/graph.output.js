jq = jQuery;
var context = '/replication';
var params = (new URL(document.location)).searchParams;

var code = params.get("code");
var name = params.get("name");
var forms = params.get("forms");
var start = params.get("startDate");
var end = params.get("endDate");
var parameters = params.get("parameters");
var min = params.get("min");
var max = params.get("max");
url = context +'/validation/filter?siteId='+code+'&forms='+forms+'&params='+parameters+'&start='+start+'&end='+end+'&min='+min+'&max='+max;



function loadValidations() {
    if (code == null)
        location.replace('index.html');
    document.getElementById('pageTitle').innerHTML = name + ' - Patient: [' + min + ', ' + max + '] - Form: ' + forms.split(',')[0] + ' - Collection Date: ' + start;
    if (code && min && max) {
        $.getJSON(url, function (rs) {
            var data=[];
            $(function () {
                $.each(rs, function (k, v) {
                    var $th = $('<th>').append(k).appendTo('#vThead');
                    $.each(v, function (i, e) {
                        if (data[i]){
                            data[i].push(e);
                        } else {
                            data[i] = [];
                            data[i].push(e);
                        }
                    });
                });
                $.each(data, function(i, arr){
                    var td = '';
                    $.each(arr, function(j, e){
                        if(Array.isArray(e))
                            e = e.join(" ");
                        td += '<td>'+ e +'</td>';
                    });
                    var $tr = $('<tr>').append(td).appendTo("#vTbody");
                });
            });
        });
    }
}
loadValidationsForGraph();
function loadValidationsForGraph() {
    document.getElementById('pageTitle').innerHTML = ' Data Comparison between Consolidated iSante and '+name+' for patient ['+min+', '+ max+']';
    var rootStyle = getComputedStyle(document.body);
    var themeColor1 = rootStyle.getPropertyValue("--theme-color-1").trim();
    var themeColor2 = rootStyle.getPropertyValue("--theme-color-2").trim();
    var themeColor3 = rootStyle.getPropertyValue("--theme-color-3").trim();
    var themeColor4 = rootStyle.getPropertyValue("--theme-color-4").trim();
    var themeColor5 = rootStyle.getPropertyValue("--theme-color-5").trim();
    var themeColor6 = rootStyle.getPropertyValue("--theme-color-6").trim();
    var themeColor1_10 = rootStyle
        .getPropertyValue("--theme-color-1-10")
        .trim();
    var themeColor2_10 = rootStyle
        .getPropertyValue("--theme-color-2-10")
        .trim();
    var themeColor3_10 = rootStyle
        .getPropertyValue("--theme-color-3-10")
        .trim();
    var primaryColor = rootStyle.getPropertyValue("--primary-color").trim();
    var foregroundColor = rootStyle
        .getPropertyValue("--foreground-color")
        .trim();
    var separatorColor = rootStyle.getPropertyValue("--separator-color").trim();

    var chartTooltip = {
        backgroundColor: foregroundColor,
        titleFontColor: primaryColor,
        borderColor: separatorColor,
        borderWidth: 0.5,
        bodyFontColor: primaryColor,
        bodySpacing: 10,
        xPadding: 15,
        yPadding: 15,
        cornerRadius: 0.15,
        displayColors: false
    };
    var centerTextPlugin = {
        afterDatasetsUpdate: function (chart) { },
        beforeDraw: function (chart) {
            var width = chart.chartArea.right;
            var height = chart.chartArea.bottom;
            var ctx = chart.chart.ctx;
            ctx.restore();

            var activeLabel = chart.data.labels[0];
            var activeValue = chart.data.datasets[0].data[0];
            var dataset = chart.data.datasets[0];
            var meta = dataset._meta[Object.keys(dataset._meta)[0]];
            var total = meta.total;

            var activePercentage = parseFloat(
                ((activeValue / total) * 100).toFixed(1)
            );
            activePercentage = chart.legend.legendItems[0].hidden
                ? 0
                : activePercentage;

            if (chart.pointAvailable) {
                activeLabel = chart.data.labels[chart.pointIndex];
                activeValue =
                    chart.data.datasets[chart.pointDataIndex].data[chart.pointIndex];

                dataset = chart.data.datasets[chart.pointDataIndex];
                meta = dataset._meta[Object.keys(dataset._meta)[0]];
                total = meta.total;
                activePercentage = parseFloat(
                    ((activeValue / total) * 100).toFixed(1)
                );
                activePercentage = chart.legend.legendItems[chart.pointIndex].hidden
                    ? 0
                    : activePercentage;
            }

            ctx.font = "36px" + " Nunito, sans-serif";
            ctx.fillStyle = primaryColor;
            ctx.textBaseline = "middle";

            var text = activePercentage + "%",
                textX = Math.round((width - ctx.measureText(text).width) / 2),
                textY = height / 2;
            ctx.fillText(text, textX, textY);

            ctx.font = "14px" + " Nunito, sans-serif";
            ctx.textBaseline = "middle";

            var text2 = activeLabel,
                textX = Math.round((width - ctx.measureText(text2).width) / 2),
                textY = height / 2 - 30;
            ctx.fillText(text2, textX, textY);

            ctx.save();
        },
        beforeEvent: function (chart, event, options) {
            var firstPoint = chart.getElementAtEvent(event)[0];

            if (firstPoint) {
                chart.pointIndex = firstPoint._index;
                chart.pointDataIndex = firstPoint._datasetIndex;
                chart.pointAvailable = true;
            }
        }
    };
    if (document.getElementById("categoryChart")) {

        var categoryChart = document.getElementById("categoryChart");
        var myDoughnutChart = new Chart(categoryChart, {
            plugins: [centerTextPlugin],
            type: "DoughnutWithShadow",
            data: {
                labels: ["Nom", "Cupcakes", "Desserts"],
                datasets: [
                    {
                        label: "",
                        borderColor: [themeColor3, themeColor2, themeColor1],
                        backgroundColor: [
                            themeColor3_10,
                            themeColor2_10,
                            themeColor1_10
                        ],
                        borderWidth: 2,
                        data: [15, 25, 20]
                    }
                ]
            },
            draw: function () {
            },
            options: {
                plugins: {
                    datalabels: {
                        display: false
                    }
                },
                responsive: true,
                maintainAspectRatio: false,
                cutoutPercentage: 80,
                title: {
                    display: false
                },
                layout: {
                    padding: {
                        bottom: 20
                    }
                },
                legend: {
                    position: "bottom",
                    labels: {
                        padding: 30,
                        usePointStyle: true,
                        fontSize: 12
                    }
                },
                tooltips: chartTooltip
            }
        });
    }
}